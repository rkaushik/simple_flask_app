from flask import Flask
import time

app = Flask(__name__)

@app.route('/<int:delay>')
def hello_world(delay):
    print("Waiting for ", delay, " seconds")
    time.sleep(delay)
    return "Hello World"

if __name__ == '__main__':
#    app.run()
#    app.run(host="your.host", port=4321, threaded=True)
#    app.run(host="your.host", port=4321, processes=1) #up to 3 processes
    app.run(threaded=False, processes=1) #up to 1 process and no thread